package se.kth.md.aide.repository.box;

import com.box.sdk.BoxFile;
import com.box.sdk.BoxFileVersion;

import java.net.URL;

public class BoxInfoWrapperForRevisionFiles {

    private BoxFile.Info fileInfo;
    private BoxFileVersion version;
    private URL previewURL;
    private URL downloadURL;

    public URL getPreviewURL() {
        return previewURL;
    }

    public void setPreviewURL(URL previewURL) {
        this.previewURL = previewURL;
    }

    public URL getDownloadURL() {
        return downloadURL;
    }

    public void setDownloadURL(URL downloadURL) {
        this.downloadURL = downloadURL;
    }

    public BoxInfoWrapperForRevisionFiles(BoxFile.Info fileInfo, BoxFileVersion version) {
        this.fileInfo = fileInfo;
        this.version = version;
    }

    public BoxInfoWrapperForRevisionFiles() { }

    public BoxFile.Info getFileInfo() {
        return fileInfo;
    }

    public void setFileInfo(BoxFile.Info fileInfo) {
        this.fileInfo = fileInfo;
    }

    public BoxFileVersion getVersion() {
        return version;
    }

    public void setVersion(BoxFileVersion version) {
        this.version = version;
    }
}
