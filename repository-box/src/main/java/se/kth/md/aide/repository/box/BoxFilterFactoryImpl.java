package se.kth.md.aide.repository.box;

import se.kth.md.aide.repository.api.FilterAbstractFactory;
import se.kth.md.aide.repository.api.PathFilter;

/**
 * Created on 12.03.17
 *
 * @author Andrew Berezovskyi (andriib@kth.se)
 * @version $version-stub$
 * @since 0.0.1
 */
public class BoxFilterFactoryImpl implements FilterAbstractFactory {
    @Override
    public PathFilter getPathFilter(final String extension) {
        return new BoxPathFilterImpl(extension);
    }
}
