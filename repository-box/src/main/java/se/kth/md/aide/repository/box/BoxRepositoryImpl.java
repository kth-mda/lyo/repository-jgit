package se.kth.md.aide.repository.box;

import com.box.sdk.BoxAPIConnection;
import com.box.sdk.BoxAPIException;
import com.box.sdk.BoxFile;
import com.box.sdk.BoxFileVersion;
import com.box.sdk.BoxFolder;
import com.box.sdk.BoxItem;
import com.box.sdk.BoxUser;
import com.google.common.collect.ImmutableMap;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.kth.md.aide.repository.api.Filter;
import se.kth.md.aide.repository.api.FilterAbstractFactory;
import se.kth.md.aide.repository.api.Repository;
import se.kth.md.aide.repository.api.RepositoryAccessException;
import se.kth.md.aide.repository.api.RepositoryFile;
import se.kth.md.aide.repository.api.Revision;
import se.kth.md.aide.repository.api.RevisionAbstractObject;

public class BoxRepositoryImpl implements Repository {
    private static final String BOX_FILE_INFO = "box.fileinfo";
    private static final String BOX_FILE_VERSION = "box.version";

    private static final Logger log = LoggerFactory.getLogger(BoxRepositoryImpl.class);

    private final BoxAPIConnection api;
    private final BoxFolder boxRootFolder;
    private final BoxFolder.Info boxRootFolderInfo;

    /**
     * The Java Box API is not well designed when it comes to handling BoxFile.Info vs.
     * BoxFileVersion.Info, so we type the second parameter of this map by 'Object'
     * that will store either an instance of BoxFile.Info or an instance of BoxFileVersion.
     */
    private HashMap<String, Object> boxFilesAndVersionsLastRevSnapshot = new HashMap<>();
    private HashMap<String, BoxFile.Info> boxFilesHeadRevision = new HashMap<>();
    private Long currentRevisionID = 0L;
    private ArrayList<Revision> revisionList = new ArrayList<>();
    private Date lastRevisionsDate;

    private final static int BOX_REVISION_UPDATE_TIMER_IN_SEC = 1000;

    // Temporary maps used for retrieving URLs from the Box APIs for a first demo of the
    // adaptor, but will be implemented in a different way later on.
    private HashMap<String, URL> boxPreviewURLs = new HashMap<>();
    private HashMap<String, URL> boxDownloadURLs = new HashMap<>();
    private final FilterAbstractFactory filterFactory = new BoxFilterFactoryImpl();

    public BoxRepositoryImpl(String token, String folder) throws IOException {

        api = new BoxAPIConnection(token);
        BoxUser.Info userInfo = BoxUser.getCurrentUser(api).getInfo();

        log.debug("Welcome, %s <%s>!\n\n", userInfo.getName(), userInfo.getLogin());

        boxRootFolder = getAdaptorRootFolder(BoxFolder.getRootFolder(api), folder);
        boxRootFolderInfo = boxRootFolder.getInfo();
        log.info("Adaptor root Box folder: %s\n\n", boxRootFolder.getInfo().getName());

        //lastFetchedRevision = new Date(0);
        //lastRevisionsDate = new Date(0);

        constructBoxRevisions();
    }

    private static BoxFolder getAdaptorRootFolder(BoxFolder parentFolder, String folderName) {
        for (BoxItem.Info itemInfo : parentFolder) {
            if (itemInfo instanceof BoxFolder.Info) {
                if (itemInfo.getName().equalsIgnoreCase(folderName)) {
                    return (BoxFolder) itemInfo.getResource();
                }
            }
        }
        throw new NoSuchElementException("Box folder not found: " + folderName);
    }

//    private static void getBoxFiles(BoxFolder folder, HashSet<BoxFile.Info> boxFilesInfo) {
//        for (BoxItem.Info itemInfo : folder) {
//            if (itemInfo instanceof BoxFolder.Info) {
//                getBoxFiles((BoxFolder) itemInfo.getResource(), boxFilesInfo);
//            } else if (itemInfo instanceof BoxFile.Info) {
//                // I have to retrieve the all the file info via this call, because of a bug in
//                // the Box SDK?
//                BoxFile.Info info = ((BoxFile) itemInfo.getResource()).getInfo();
//                boxFilesInfo.add(info);
//            }
//        }
//    }

    private static void getBoxFiles(BoxFolder folder, HashMap<String, BoxFile.Info> boxFiles) {
        for (BoxItem.Info itemInfo : folder) {
            if (itemInfo instanceof BoxFolder.Info) {
                getBoxFiles((BoxFolder) itemInfo.getResource(), boxFiles);
            } else if (itemInfo instanceof BoxFile.Info) {
                // I have to retrieve the all the file info via this call, because of a bug in
                // the Box SDK?
                BoxFile.Info info = ((BoxFile) itemInfo.getResource()).getInfo();
                boxFiles.put(info.getID(), info);
            }
        }
    }

    private void constructBoxRevisions() {
        // See comment above why we cannot type more precisely the instances we are dealing with here.
        List<Object> newlyCreatedAndUpdatedFiles = new ArrayList<Object>();

        getBoxFiles(boxRootFolder, boxFilesHeadRevision);

        System.out.println();
        for (BoxFile.Info info : boxFilesHeadRevision.values()) {
            log.trace("☞ file='{}'; id='{}'; v='{}'", info.getName(), info.getID(),
                    info.getVersionNumber());
//            System.out.println("FILE: " + info.getName() + ", ID: " + info.getID() + ", V: " + info.getVersionNumber());
            for (BoxFileVersion version : info.getResource().getVersions()) {
                log.trace("  file='{}'; id='{}'; v='{}'", version.getName(), version.getFileID(),
                        version.getVersionID());
//                System.out.println("    VERSION, file name: " + version.getName()
//                        + "\n        FILEID: " + version.getFileID()
//                        + "\n        VERSION ID: " + version.getVersionID());
            }
        }
        System.out.println();

        for (BoxFile.Info fileInfo : boxFilesHeadRevision.values()) {
            String fileID = fileInfo.getID();

            if (!boxFilesAndVersionsLastRevSnapshot.containsKey(fileID)) {
                newlyCreatedAndUpdatedFiles.add(fileInfo);
            }

            for (BoxFileVersion version : fileInfo.getResource().getVersions()) {
                Object obj = boxFilesAndVersionsLastRevSnapshot.get(fileID);
                if (obj == null) {
                    newlyCreatedAndUpdatedFiles.add(version);
                } else if (obj instanceof BoxFile.Info) {
                    newlyCreatedAndUpdatedFiles.add(version);
                } else if (obj instanceof BoxFileVersion) {
                    BoxFileVersion previousVersion = (BoxFileVersion)obj;
                    if (version.getCreatedAt().after(previousVersion.getCreatedAt())) {
                        newlyCreatedAndUpdatedFiles.add(version);
                    }
                }
            }
            // This has nothing to do with version management, but we do not want to make these costly calls
            // to the Box API repetitively in the FileManagementResourceExtractor class.
            // It will be implemented in a different way later on.
            URL previewURL = null;
            try {
                previewURL = fileInfo.getResource().getPreviewLink();
                boxPreviewURLs.put(fileID, previewURL);
            } catch (BoxAPIException e) {
                // An error (code 415) is raised by the Box API when there is no preview available
                // for unsupported file extensions, in that case, we simply silently ignore it
                // e.printStackTrace();
            }
            boxDownloadURLs.put(fileID, fileInfo.getResource().getDownloadURL());

        }

        newlyCreatedAndUpdatedFiles.sort(new BoxItemTimestampComparator());

        for (Object o : newlyCreatedAndUpdatedFiles) {
            Revision newRev;
            ZonedDateTime zd = null;

            if (o instanceof BoxFile.Info) {
                BoxFile.Info info = (BoxFile.Info)o;
                String fileId = info.getID();
                zd = ZonedDateTime.ofInstant(info.getCreatedAt().toInstant(),
                        ZoneId.systemDefault());
                boxFilesAndVersionsLastRevSnapshot.put(fileId, info);
            } else if (o instanceof BoxFileVersion) {
                BoxFileVersion version = (BoxFileVersion)o;
                String versionId = version.getVersionID();
                String fileId = version.getFileID();
                zd = ZonedDateTime.ofInstant(version.getCreatedAt().toInstant(),
                        ZoneId.systemDefault());
                if (boxFilesAndVersionsLastRevSnapshot.containsKey(fileId)) {
                    boxFilesAndVersionsLastRevSnapshot.replace(fileId, version);
                } else {
                    boxFilesAndVersionsLastRevSnapshot.put(fileId, version);
                }
            }

            HashMap<String, Object> newBoxRevisionSnapshot = new HashMap<>(boxFilesAndVersionsLastRevSnapshot);

            BoxInfoWrapperForRevisions wrapper = new BoxInfoWrapperForRevisions(newBoxRevisionSnapshot);

            newRev = new Revision(currentRevisionID.toString(), zd, wrapper);
            //newRev = new Revision(currentRevisionID.toString(), zd, null);

            currentRevisionID++;

            lastRevisionsDate = new Date();

            revisionList.add(newRev);
            //revisionList.put(newRev.getId(), newRev);
        }
    }

    @Override
    public List<Revision> getRevisions() {
        Date currentTime = new Date();

        if (currentTime.getTime() - lastRevisionsDate.getTime() > BOX_REVISION_UPDATE_TIMER_IN_SEC * 1000) {
            constructBoxRevisions();
        }

        // DEBUG
        Revision lastRev = revisionList.get(revisionList.size()-1);
        System.out.println("\nDEBUG LAST BOX REVISION ID: " + lastRev.getId());
        BoxInfoWrapperForRevisions revWrapper = (BoxInfoWrapperForRevisions)lastRev.getRevObject();
        HashMap<String, Object> boxRevisionSnapshot = revWrapper.getRevisionSnapshot();
        for (Object obj : boxRevisionSnapshot.values()) {
            String fileID;
            BoxFileVersion version = null;
            if (obj instanceof BoxFile.Info) {
                fileID = ((BoxFile.Info)obj).getID();
            } else {
                fileID = ((BoxFileVersion)obj).getFileID();
                version = (BoxFileVersion)obj;
            }
            BoxFile.Info info = boxFilesHeadRevision.get(fileID);
            System.out.println("    FILE: " + info.getName() + ", ID: " + info.getID());
            if (version != null) {
                System.out.println("        WITH VERSION ID: " + version.getVersionID());
            }
        }
        System.out.println();

        //return new ArrayList<Revision>(revisionList.values());
        return revisionList;
    }

    private BoxFileVersion getPreviousVersion(BoxFileVersion[] versions, BoxFileVersion version) {
        for (int i = 1; i < versions.length; i++) {
            if (versions[i].getVersionID().equals(version.getVersionID())) {
                return versions[i-1];
            }
        }
        return null;
    }

    @Override
    public List<RevisionAbstractObject> getRevisionFiles(Revision rev, String fileExtension)
            throws RepositoryAccessException {
        return getRevisionObjects(rev, getFilterFactory().getPathFilter(fileExtension));
    }

    @Override
    public List<RevisionAbstractObject> getRevisionObjects(final Revision rev, final Filter filter)
            throws RepositoryAccessException {
        List<RevisionAbstractObject> repositoryFiles = new ArrayList<>();
        RevisionAbstractObject revisionAbstractObject;
        String absolutePath;
        BoxInfoWrapperForRevisions revWrapper = (BoxInfoWrapperForRevisions)rev.getRevObject();
        HashMap<String, Object> boxRevisionSnapshot = revWrapper.getRevisionSnapshot();

        for (Object obj : boxRevisionSnapshot.values()) {
            String fileID;
            BoxFileVersion version = null;
            if (obj instanceof BoxFile.Info) {
                fileID = ((BoxFile.Info)obj).getID();
            } else {
                fileID = ((BoxFileVersion)obj).getFileID();
                version = (BoxFileVersion)obj;
            }
            // We reuse this map boxFilesHeadRevision in order to get a direct access to the BoxFile.Info we need,
            // otherwise, we need to invoke again a costly method call to the Box API to get this info again
            // (because of a bug of the Box Java SDK?)
            BoxFile.Info fileInfo = boxFilesHeadRevision.get(fileID);

            //absolutePath = getAbsolutePath(fileInfo).substring(boxRootFolder.getInfo().getName().length() + 2);
            absolutePath = getAbsolutePath(fileInfo).substring(boxRootFolderInfo.getName().length() + 2);

            BoxInfoWrapperForRevisionFiles newInfoWrapper = new BoxInfoWrapperForRevisionFiles(fileInfo, version);


            // Temporary, will be implemented in a different way later on
            //newInfoWrapper.setPreviewURL(boxPreviewURLs.get(fileID));
            //newInfoWrapper.setDownloadURL(boxDownloadURLs.get(fileID));

            revisionAbstractObject = new RepositoryFile(rev, absolutePath,
                    ImmutableMap.of(BOX_FILE_INFO, fileInfo,
                            BOX_FILE_VERSION, version));
            repositoryFiles.add(revisionAbstractObject);
        }

        return repositoryFiles;
    }

    @Override
    public InputStream getObjectStream(final RevisionAbstractObject fileObject) throws IOException {
        if (!fileObject.getMetadata().containsKey(BOX_FILE_INFO)) {
            throw new IllegalArgumentException(
                    "RevisionAbstractObject must be a Box file with Info metadata object");
        }
        BoxFile.Info fileInfo = (BoxFile.Info) fileObject.getMetadata().get(BOX_FILE_INFO);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        fileInfo.getResource().download(outputStream);
        return new ByteArrayInputStream(outputStream.toByteArray());
    }

    @Override
    public FilterAbstractFactory getFilterFactory() {
        return filterFactory;
    }

    private String getAbsolutePath(BoxFile.Info fileInfo) {
        // @ndrew: more compact alternative
        return "/" + String.join("/", fileInfo.getPathCollection()
                .stream()
                .map(info -> info.getName())
                .collect(Collectors.toList()));
//        String absolutePath = "/";
//        for (int i = 1; i < fileInfo.getPathCollection().size(); i++) {
//            absolutePath += fileInfo.getPathCollection().get(i).getName() + "/";
//        }
//        return absolutePath;
    }

    private class BoxItemTimestampComparator implements Comparator {
        @Override
        public int compare(Object o1, Object o2) {
            ZonedDateTime dateO1 = null;
            ZonedDateTime dateO2 = null;
            if (o1 instanceof BoxFile.Info) {
                dateO1 = ZonedDateTime.ofInstant(((BoxFile.Info)o1).getCreatedAt().toInstant(),
                            ZoneId.systemDefault());
            } else if (o1 instanceof BoxFileVersion) {
                dateO1 = ZonedDateTime.ofInstant(((BoxFileVersion)o1).getCreatedAt().toInstant(),
                        ZoneId.systemDefault());
            }
            if (o2 instanceof BoxFile.Info) {
                dateO2 = ZonedDateTime.ofInstant(((BoxFile.Info)o2).getCreatedAt().toInstant(),
                        ZoneId.systemDefault());
            } else if (o2 instanceof BoxFileVersion) {
                dateO2 = ZonedDateTime.ofInstant(((BoxFileVersion)o2).getCreatedAt().toInstant(),
                        ZoneId.systemDefault());
            }
            if (dateO1 == null || dateO2 == null) {
                throw new ClassCastException("Objects to compare must be instances of BoxFile.Info or BoxFileVersion.");
            }

            if (dateO1.isBefore(dateO2)) {
                return -1;
            } else if (dateO1.isAfter(dateO2)) {
                return 1;
            } else {
                return 0;
            }
        }
    }
}
