package se.kth.md.aide.repository.box;

import java.util.HashMap;

public class BoxInfoWrapperForRevisions {

    private HashMap<String, Object> revisionSnapshot;

    public BoxInfoWrapperForRevisions(HashMap<String, Object> revisionSnapshot) {

        this.revisionSnapshot = revisionSnapshot;
    }

    public HashMap<String, Object> getRevisionSnapshot() {
        return revisionSnapshot;
    }

    public void setRevisionSnapshot(HashMap<String, Object> revisionSnapshot) {
        this.revisionSnapshot = revisionSnapshot;
    }

}
