package se.kth.md.aide.repository.box;

import se.kth.md.aide.repository.api.PathFilter;

/**
 * Created on 12.03.17
 *
 * @author Andrew Berezovskyi (andriib@kth.se)
 * @version $version-stub$
 * @since 0.0.1
 */
public class BoxPathFilterImpl implements PathFilter {
    private final String extension;

    public BoxPathFilterImpl(final String extension) {
        this.extension = extension;
    }

    @Override
    public boolean isValid(final String path, final boolean isDirectory) {
        // TODO /Andrew 13.03.17: Frederic, you need to decide whether to allow directories
        return path.endsWith(extension);
    }

    public String getExtension() {
        return extension;
    }
}
