package se.kth.md.aide.repository.box;

import org.junit.Ignore;
import org.junit.Test;
import se.kth.md.aide.repository.api.Repository;
import se.kth.md.aide.repository.api.RepositoryAccessException;
import se.kth.md.aide.repository.api.Revision;
import se.kth.md.aide.repository.api.RepositoryFile;

import java.io.IOException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import se.kth.md.aide.repository.api.RevisionAbstractObject;

/**
 * @author Andrew Berezovskyi <andriib@kth.se>
 * @since 2016-06-10
 */
public class BoxRepositoryTest {

    private String token = "";
    private String folder = "";

    /**
     * It should be possible to open the Git repository
     */
    @Test
    public void testInitializationOk() throws IOException {
        //BoxRepositoryImpl repository = getBoxRepository();
    }

    private BoxRepositoryImpl getBoxRepository() throws IOException {
        return new BoxRepositoryImpl(token, folder);
    }

    @Test
    @Ignore
    public void testRepositoryHasRevision() throws IOException {
        Repository repository = getBoxRepository();
        List<Revision> revisions = repository.getRevisions();
        assertThat(revisions.size()).isGreaterThan(0);
    }

    @Test
    @Ignore
    public void testRepositoryHasReqifFiles() throws IOException {
        Repository repository = getBoxRepository();
        List<RevisionAbstractObject> revisionFiles = getLastRevisionObjects(repository);
        assertThat(revisionFiles.size()).isEqualTo(2);
    }

    private List<RevisionAbstractObject> getLastRevisionObjects(Repository repository)
            throws RepositoryAccessException {
        List<Revision> revisions = repository.getRevisions();
        Revision revision = revisions.get(0);
        return repository.getRevisionFiles(revision, ".reqif");
    }

    @Test
    @Ignore
    public void testRepositoryFileStreams() throws IOException {
        Repository repository = getBoxRepository();
        List<RevisionAbstractObject> revisionFiles = getLastRevisionObjects(repository);
        assertThat(revisionFiles.size()).isGreaterThan(
                0); // dup, just to make sure this test fails if need be
        for (RevisionAbstractObject obj : revisionFiles) {
            assertThat(repository.getObjectStream(obj).available()).isGreaterThan(0)
                    .describedAs(String.format("%s is" + " empty", obj.getPath()));
        }
    }
}
