# AIDE repository libraries

## Overview

This project contains a generic repository Java API (`respository-api`) and implementations (providers) for:

- Box
- Git (via Eclipse JGit)

There is also a provider for SVN but due to licensing concerns, it's provided in a [separate repository](https://gitlab.com/kth-mda/lyo/aide-repository-svnkit).

## License

> Copyright 2023 KTH Royal Institute of Technology
> 
> Licensed under the EUPL-1.2 or later.
> 
> SPDX-License-Identifier: EUPL-1.2