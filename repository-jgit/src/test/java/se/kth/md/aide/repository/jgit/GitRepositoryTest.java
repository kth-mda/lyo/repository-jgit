package se.kth.md.aide.repository.jgit;

import java.io.IOException;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;
import se.kth.md.aide.repository.api.RepositoryAccessException;
import se.kth.md.aide.repository.api.Revision;
import se.kth.md.aide.repository.api.RevisionAbstractObject;

/**
 * @author Andrew Berezovskyi <andriib@kth.se>
 * @since 2016-06-10
 */
public class GitRepositoryTest {

    private static final String REPOSITORY = "/repo/";

    /**
     * It should be possible to open the Git repository
     */
    @Test
    public void testRepositoryIsFound() throws IOException {
        GitRepository repository = getGitRepository();
    }

    /**
     * It should be possible to list the revisions in the repository
     */
    @Test
    public void testRepositoryHasRevision() throws IOException {
        GitRepository repository = getGitRepository();
        List<Revision> revisions = repository.getRevisions();
        assertThat(revisions.size()).isGreaterThan(0);
    }

    @Test
    public void testRepositoryHasReqifFiles() throws IOException {
        GitRepository repository = getGitRepository();
        List<RevisionAbstractObject> revisionFiles = getLastRevisionObjects(repository);
        assertThat(revisionFiles.size()).isEqualTo(2);
    }

    @Test
    public void testRepositoryFileStreams() throws IOException {
        GitRepository repository = getGitRepository();
        List<RevisionAbstractObject> revisionFiles = getLastRevisionObjects(repository);
        assertThat(revisionFiles.size()).isGreaterThan(
                0); // dup, just to make sure this test fails if need be
        for (RevisionAbstractObject obj : revisionFiles) {
            assertThat(repository.getObjectStream(obj).available()).isGreaterThan(0)
                    .describedAs(String.format("%s is" + " empty", obj.getPath()));
        }
    }

    private GitRepository getGitRepository() throws IOException {
        String path = this.getClass().getResource(REPOSITORY).getPath();
        return new GitRepository(path);
    }

    private List<RevisionAbstractObject> getLastRevisionObjects(GitRepository repository)
            throws RepositoryAccessException {
        List<Revision> revisions = repository.getRevisions();
        Revision revision = revisions.get(0);
        return repository.getRevisionFiles(revision, ".reqif");
    }
}
