package se.kth.md.aide.repository.jgit;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ObjectLoader;
import org.eclipse.jgit.lib.ObjectReader;
import org.eclipse.jgit.lib.PersonIdent;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.treewalk.TreeWalk;
import org.eclipse.jgit.treewalk.filter.PathFilter;
import org.eclipse.jgit.treewalk.filter.PathSuffixFilter;
import org.eclipse.jgit.treewalk.filter.TreeFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.kth.md.aide.repository.api.EmptyFilter;
import se.kth.md.aide.repository.api.Filter;
import se.kth.md.aide.repository.api.FilterAbstractFactory;
import se.kth.md.aide.repository.api.Repository;
import se.kth.md.aide.repository.api.RepositoryAccessException;
import se.kth.md.aide.repository.api.RepositoryFile;
import se.kth.md.aide.repository.api.Revision;
import se.kth.md.aide.repository.api.RevisionAbstractObject;
import se.kth.md.aide.repository.api.RevisionObjectCommonAttributes;
import se.kth.md.aide.repository.api.RevisionObjectMetaDataConstants;

/**
 * GitRepository is .
 *
 * @author Andrew Berezovskyi (andriib@kth.se)
 * @since 0.1.0
 */
public class GitRepository implements Repository {

    private static final Object lock = new Object();
    private final Logger logger = LoggerFactory.getLogger(GitRepository.class);
    private final org.eclipse.jgit.lib.Repository repo;
    private final Git git;
    private GitFilterAbstractFactoryImpl abstractFactory;

    public GitRepository(String directory) throws IOException {
        File repoDirectory = new File(directory);
        git = Git.open(repoDirectory);
        repo = git.getRepository();
    }

    @Override
    public List<Revision> getRevisions() {
        try {
            Iterable<RevCommit> logs = git.log().call();
            Iterator<RevCommit> revCommitIterator = logs.iterator();
            Iterable<RevCommit> iterable = () -> revCommitIterator;
            Stream<RevCommit> commitStream = StreamSupport.stream(iterable.spliterator(), false);

            return commitStream.map(this::buildRevision).collect(Collectors.toList());
        } catch (GitAPIException e) {
            return new ArrayList<>();
        }
    }

    public List<RevisionAbstractObject> getRevisionFiles(Revision rev, String fileExtension)
            throws RepositoryAccessException {
        PathSuffixFilter filter = PathSuffixFilter.create(fileExtension);
        return fetchRevisionWithSuffixFilter(rev, filter);
    }

    @Override
    public List<RevisionAbstractObject> getRevisionObjects(final Revision rev, final Filter filter)
            throws RepositoryAccessException {
        ArrayList<RevisionAbstractObject> objectArrayList = new ArrayList<>();
        if (filter instanceof GitPathFilter) {
            GitPathFilter pathFilter = (GitPathFilter) filter;
            objectArrayList.addAll(
                    fetchRevisionWithSuffixFilter(rev, pathFilter.getGitTreeFilter()));
            return objectArrayList;
        } else if(filter instanceof EmptyFilter) {
            objectArrayList.addAll(
                fetchRevisionWithSuffixFilter(rev, TreeFilter.ALL));
        }
        throw new IllegalArgumentException(
                "Use only filters produced by the getFilterFactory() of this repository");
    }

    @Override
    public InputStream getObjectStream(final RevisionAbstractObject fileObject)
            throws IOException, NoSuchElementException {
        ObjectId id = repo.resolve(fileObject.getRevision().getId());
        try (ObjectReader reader = this.repo.newObjectReader()) {
            RevWalk revWalk = new RevWalk(reader);
            RevCommit revCommit = revWalk.parseCommit(id);
            RevTree revTree = revCommit.getTree();

            TreeWalk treeWalk = new TreeWalk(repo);
            treeWalk.addTree(revTree);
            treeWalk.setRecursive(true);
            treeWalk.setFilter(PathFilter.create(fileObject.getPath()));

            if (!treeWalk.next()) {
                throw new NoSuchElementException("Did not find expected file");
            }

            logger.trace("Fetching object stream of '{}'", treeWalk.getPathString());
            ObjectId objectId = treeWalk.getObjectId(0);
            ObjectLoader loader = repo.open(objectId);
            return loader.openStream();
        }
    }

    @Override
    public FilterAbstractFactory getFilterFactory() {
        if (abstractFactory == null) {
            synchronized (lock) {
                // TODO: 12.03.17 maybe need to make it volatile
                if (abstractFactory == null) {
                    abstractFactory = new GitFilterAbstractFactoryImpl();
                }
            }
        }
        return abstractFactory;
    }

    private List<RevisionAbstractObject> fetchRevisionWithSuffixFilter(final Revision rev,
            final TreeFilter filter) throws RepositoryAccessException {
        try {
            ObjectId id = repo.resolve(rev.getId());
            try (ObjectReader reader = this.repo.newObjectReader()) {
                List<RevisionAbstractObject> repositoryFiles = new ArrayList<>();
                RevWalk revWalk = new RevWalk(reader);
                RevCommit revCommit = revWalk.parseCommit(id);
                RevTree revTree = revCommit.getTree();

                TreeWalk treeWalk = new TreeWalk(repo);
                treeWalk.addTree(revTree);
                treeWalk.setRecursive(true);
                treeWalk.setFilter(filter);

                while (treeWalk.next()) {
                    if (treeWalk.isSubtree()) {
                        logger.trace("dir: {}", treeWalk.getPathString());
                        treeWalk.enterSubtree();
                    } else {
                        logger.trace("Parsing file '{}'", treeWalk.getPathString());

                        RevisionObjectCommonAttributes atts = new RevisionObjectCommonAttributes();
                        ObjectLoader loader = this.repo.open(treeWalk.getObjectId(0));
                        atts.setSize(loader.getSize());
                        Map<String, Object> metadata = new HashMap<>();
                        metadata.put(RevisionObjectMetaDataConstants.RevisionObjectBasicAttributes_key, atts);

                        RepositoryFile repositoryFile = new RepositoryFile(rev,
                                treeWalk.getPathString(), metadata);
                        repositoryFiles.add(repositoryFile);
                    }
                }
                return repositoryFiles;
            }
        } catch (IOException e) {
            throw new RepositoryAccessException(e);
        }
    }

    private Revision buildRevision(RevCommit commit) {
        return new Revision(getCommitId(commit), getTimestamp(commit));
    }

    private ZonedDateTime getTimestamp(RevCommit commit) {
        PersonIdent authorIdent = commit.getAuthorIdent();
        return authorIdent.getWhen().toInstant().atZone(authorIdent.getTimeZone().toZoneId());
    }

    private String getCommitId(RevCommit commit) {
        return commit.getId().getName();
    }
}
