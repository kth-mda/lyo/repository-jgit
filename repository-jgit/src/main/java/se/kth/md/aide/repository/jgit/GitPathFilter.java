package se.kth.md.aide.repository.jgit;

import org.eclipse.jgit.treewalk.filter.PathSuffixFilter;
import org.eclipse.jgit.treewalk.filter.TreeFilter;
import se.kth.md.aide.repository.api.PathFilter;

/**
 * Created on 10.03.17
 *
 * @author Andrew Berezovskyi (andriib@kth.se)
 * @version $version-stub$
 * @since 0.0.1
 */
public class GitPathFilter implements PathFilter {
    private final String extension;

    GitPathFilter(final String extension) {
        this.extension = extension;
    }

    @Override
    public boolean isValid(final String path, final boolean isDirectory) {
        return path.endsWith(extension) && !isDirectory;
    }

    public TreeFilter getGitTreeFilter() {
        return PathSuffixFilter.create(extension);
    }
}
