.PHONY: repo

repo:
	mkdir -p repository-jgit/src/test/resources/repo
	cd repository-jgit/src/test/resources/repo && \
	 git init && \
	 touch test.file && \
	 touch reqif.txt && \
	 echo "test" > req.reqif && \
	 mkdir -p backup/ && \
	 echo "test" > backup/req.reqif && \
	 git add -A && \
	 git commit -m "test files added"


initgit:
	git config --global user.email "build@gitlab.com"
	git config --global user.name "Gitlab Build Agent"
