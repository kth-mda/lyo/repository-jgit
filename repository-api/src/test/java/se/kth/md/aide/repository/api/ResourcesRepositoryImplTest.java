package se.kth.md.aide.repository.api;

import java.io.InputStream;
import java.util.List;
import java.util.Properties;
import org.junit.Test;
import static org.assertj.core.api.Assertions.*;

/**
 * Created on 13.03.17
 * @author Andrew Berezovskyi (andriib@kth.se)
 * @version $version-stub$
 * @since 0.0.1
 */
public class ResourcesRepositoryImplTest {
  public static final String TEST_EXT = ".properties";
  private final Repository repository = new ResourcesRepositoryImpl();

  @Test
  public void getRevisions() throws Exception {
    assertThat(repository.getRevisions()).hasSize(1);
  }

  @Test(expected = IllegalArgumentException.class)
  public void getRevisionFilesRequiresRevision() throws Exception {
    repository.getRevisionFiles(null, TEST_EXT);
  }

  @Test
  public void getRevisionFilesNotNull() throws Exception {
    List<Revision> revisions = repository.getRevisions();
    Revision revision = revisions.get(0); // always do forloop, I know that there is only one rev

    List<RevisionAbstractObject> revisionFiles = repository.getRevisionFiles(revision, TEST_EXT);
    assertThat(revisionFiles).hasSize(3);
  }


  @Test
  public void getRevisionFilesCanBeParsed() throws Exception {
    List<Revision> revisions = repository.getRevisions();
    Revision revision = revisions.get(0); // always do forloop, I know that there is only one rev
    List<RevisionAbstractObject> revisionFiles = repository.getRevisionFiles(revision, TEST_EXT);
    RevisionAbstractObject abstractObject = revisionFiles.get(0);// f1.properties
    InputStream inputStream = repository.getObjectStream(abstractObject);

    Properties properties = new Properties();
    properties.load(inputStream);

    assertThat(properties.getProperty("test")).isEqualToIgnoringCase("ok");
  }
}
