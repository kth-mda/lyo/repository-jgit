import java.time.ZoneId;
import java.time.ZonedDateTime;
import static org.junit.Assert.*;
import org.junit.Test;
import se.kth.md.aide.repository.api.Revision;
import static org.hamcrest.CoreMatchers.*;
/**
 * TestReview is .
 * @author Andrii Berezovskyi (andriib@kth.se)
 * @version $Id$
 * @since 0.13.0
 */
public class TestRevision {
    @Test
    public void testDummy() throws Exception {
        Revision dummy = Revision.dummy();
        ZonedDateTime utcEpoch = ZonedDateTime.of(1970, 1, 1, 0, 0, 1, 0, ZoneId.of("UTC"));

        assertTrue(dummy.getTimestamp().isBefore(utcEpoch));
    }
}
