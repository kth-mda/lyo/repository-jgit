package se.kth.md.aide.repository.api;

/**
 * Created on 12.03.17
 *
 * @author Andrew Berezovskyi (andriib@kth.se)
 * @version $version-stub$
 * @since 0.0.1
 */
public final class EmptyFilter implements Filter {
}
