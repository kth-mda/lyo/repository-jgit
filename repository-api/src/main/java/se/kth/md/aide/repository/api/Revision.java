package se.kth.md.aide.repository.api;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * Revision represents a generic revision identifier.
 *
 * @author Andrew Berezovskyi (andriib@kth.se)
 * @since 2016-06-10
 */
public class Revision {
    private final String        id;
    private final ZonedDateTime timestamp;
    private final Object revObject;

    public Revision(String id) {
        this(id, null);
    }

    public Revision(String id, ZonedDateTime timestamp) {
        this.id = id;
        this.timestamp = timestamp;
        revObject = null;
    }

    public Revision(String id, ZonedDateTime timestamp, Object revObject) {
        this.id = id;
        this.timestamp = timestamp;
        this.revObject = revObject;
    }

    public static Revision dummy() {
        return new Revision(null, ZonedDateTime.ofLocal(LocalDateTime.MIN, ZoneId.of("UTC"), null));
    }

    public String getId() {
        return id;
    }

    public ZonedDateTime getTimestamp() {
        return timestamp;
    }

    public Object getRevObject() { return revObject; }

    @Override
    public String toString() {
        /*final StringBuilder sb = new StringBuilder("Revision{");
        sb.append("id='").append(id.substring(0, 7)).append('\'');
        sb.append(", timestamp=").append(timestamp.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
        sb.append('}');
        return sb.toString();
        */
    	return ("revision, ID: " + id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Revision revision = (Revision) o;
        return Objects.equals(id, revision.id) && Objects.equals(timestamp, revision.timestamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, timestamp);
    }
}
