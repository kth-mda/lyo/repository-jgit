package se.kth.md.aide.repository.api;

/**
 * Created on 17.03.17
 *
 * @author Frederic Loiret (loiret@kth.se)
 * @version $version-stub$
 */
public class RevisionObjectCommonAttributes {

    private long size;

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

}
