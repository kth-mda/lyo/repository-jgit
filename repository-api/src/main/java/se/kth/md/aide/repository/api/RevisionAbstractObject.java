package se.kth.md.aide.repository.api;

import com.google.common.collect.ImmutableMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Created on 06.03.17
 *
 * @author Andrew Berezovskyi (andriib@kth.se)
 * @version $version-stub$
 * @since 0.0.1
 */
public abstract class RevisionAbstractObject {
    protected final Revision revision;
    protected final String path;
    protected final Map<String, Object> metadata;

    public RevisionAbstractObject(final Revision revision, final String path) {
        this(revision, path, new HashMap<>());
    }

    public RevisionAbstractObject(final Revision revision, final String path,
            final Map<String, Object> metadata) {
        this.revision = revision;
        this.path = path;
        this.metadata = metadata;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RevisionedObject{");
        sb.append(getPath());
        sb.append("@").append(getRevision()).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPath());
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        final RevisionAbstractObject that = (RevisionAbstractObject) o;
        return Objects.equals(revision, that.revision) && Objects.equals(path, that.path);
    }

    /**
     * @return immutable copy of the metadata
     */
    public Map<String, Object> getMetadata() {
        return ImmutableMap.copyOf(this.metadata);
    }

    public Revision getRevision() {
        return revision;
    }

    public String getPath() {
        return path;
    }
}
