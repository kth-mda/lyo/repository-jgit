package se.kth.md.aide.repository.api;

/**
 * Created on 13.03.17
 *
 * @author Andrew Berezovskyi (andriib@kth.se)
 * @version $version-stub$
 * @since 0.0.1
 */
public interface PathFilter extends Filter {
    boolean isValid(String path, boolean isDirectory);

    default boolean isValid(RevisionAbstractObject obj) {
        if (obj instanceof RepositoryFile) {
            return isValid(obj.getPath(), false);
        } else {
            return isValid(obj.getPath(), true);
        }
    }
}
