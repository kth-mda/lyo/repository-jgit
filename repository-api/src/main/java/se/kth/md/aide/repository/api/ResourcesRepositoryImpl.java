package se.kth.md.aide.repository.api;

import com.google.common.collect.ImmutableList;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Repository based on the '/src/{main,test}/resources/repository' contents.
 * @author Andrew Berezovskyi (andriib@kth.se)
 * @version $version-stub$
 * @since 0.0.1
 */
public class ResourcesRepositoryImpl implements Repository {

  public static final Revision RESOURCES_REV = new Revision("resources");
  public static final String REPOSITORY_SUBFOLDER = "repository";
  private final static Logger log = LoggerFactory.getLogger(ResourcesRepositoryImpl.class);

  /**
   * Contains a single revision corresponding to the current directory state.
   */
  @Override
  public List<Revision> getRevisions() {
    return ImmutableList.of(RESOURCES_REV);
  }

  @Override
  public List<RevisionAbstractObject> getRevisionFiles(final Revision rev, final String extension)
    throws RepositoryAccessException {
    return getRevisionObjects(rev, getFilterFactory().getPathFilter(extension));
  }

  @Override
  public List<RevisionAbstractObject> getRevisionObjects(final Revision rev, final Filter filter)
    throws RepositoryAccessException {
    if (!RESOURCES_REV.equals(rev)) {
      throw new IllegalArgumentException("Wrong Revision object was passed. Use getRevisions()");
    }
    try {
      Path repository = Paths.get(ClassLoader.getSystemResource(REPOSITORY_SUBFOLDER).toURI());
      List<RevisionAbstractObject> filesInFolder = Files.walk(repository)
        .filter(Files::isRegularFile)
        .map(Path::toFile)
        .map(file -> new RepositoryFile(rev, file.getAbsolutePath()))
        .collect(Collectors.toList());
      return filesInFolder;
    } catch (IOException | URISyntaxException e) {
      throw new RepositoryAccessException(e);
    }
  }

  @Override
  public InputStream getObjectStream(final RevisionAbstractObject fileObject) throws IOException {
    return Files.newInputStream(Paths.get(fileObject.getPath()));
  }

  @Override
  public FilterAbstractFactory getFilterFactory() {
    return extension -> (path, isDirectory) -> path.endsWith(extension) && !isDirectory;
  }
}
