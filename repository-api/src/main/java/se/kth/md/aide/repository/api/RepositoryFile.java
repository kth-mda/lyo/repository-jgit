package se.kth.md.aide.repository.api;

import java.util.Map;

/**
 * RevisionedObject is an interface to any file that has a {@link Revision} information attached.
 *
 * @author Andrew Berezovskyi (andriib@kth.se)
 * @since 2016-06-10
 */
// TODO: 12.03.17 I don't see any reason to keep this class around, it has no extra fields
public class RepositoryFile extends RevisionAbstractObject {

    public RepositoryFile(Revision revision, String path) {
        super(revision, path);
    }

    /**
     * Prefer this ctor to others.
     *
     * @param revision
     * @param path
     * @param metadata
     */
    public RepositoryFile(final Revision revision, final String path,
            final Map<String, Object> metadata) {
        super(revision, path, metadata);
    }
}
