package se.kth.md.aide.repository.api;

/**
 * Created on 17.03.17
 *
 * @author Frederic Loiret (loiret@kth.se)
 * @version $version-stub$
 */
public class RevisionObjectMetaDataConstants {

    public static final String RevisionObjectBasicAttributes_key = "RevisionObjectBasicAttributes_key";

}
