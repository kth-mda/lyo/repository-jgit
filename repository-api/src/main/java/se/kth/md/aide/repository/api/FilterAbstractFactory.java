package se.kth.md.aide.repository.api;

/**
 * Created on 10.03.17
 *
 * @author Andrew Berezovskyi (andriib@kth.se)
 * @version $version-stub$
 * @since 0.0.1
 */
public interface FilterAbstractFactory {
    default Filter emptyFilter() {
        return new EmptyFilter();
    }

    PathFilter getPathFilter(String extension);
}
