package se.kth.md.aide.repository.api;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Repository is an interface to retrieve revisioned files.
 *
 * @author Andrew Berezovskyi (andriib@kth.se)
 * @since 0.1.0
 */
public interface Repository {

    List<Revision> getRevisions();

    @Deprecated
    List<RevisionAbstractObject> getRevisionFiles(Revision rev, String extension)
            throws RepositoryAccessException;

    List<RevisionAbstractObject> getRevisionObjects(Revision rev, Filter filter)
            throws RepositoryAccessException;

    InputStream getObjectStream(RevisionAbstractObject fileObject) throws IOException;

    FilterAbstractFactory getFilterFactory();
}
