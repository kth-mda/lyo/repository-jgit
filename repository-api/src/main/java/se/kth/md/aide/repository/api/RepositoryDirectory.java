package se.kth.md.aide.repository.api;

import java.util.Map;

/**
 * Created on 06.03.17
 *
 * @author Andrew Berezovskyi (andriib@kth.se)
 * @version $version-stub$
 * @since 0.0.1
 */
public class RepositoryDirectory extends RevisionAbstractObject {
    public RepositoryDirectory(final Revision revision, final String path) {
        super(revision, path);
    }

    public RepositoryDirectory(final Revision revision, final String path,
            final Map<String, Object> metadata) {
        super(revision, path, metadata);
    }
}
