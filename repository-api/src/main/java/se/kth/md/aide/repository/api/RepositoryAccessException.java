package se.kth.md.aide.repository.api;

import java.io.IOException;

/**
 * RepositoryAccessException is a general exception for repository I/O operations.
 *
 * @author Andrew Berezovskyi (andriib@kth.se)
 * @since 2016-06-13
 */
public class RepositoryAccessException extends IOException {
    public RepositoryAccessException() {
    }

    public RepositoryAccessException(String message) {
        super(message);
    }

    public RepositoryAccessException(String message, Throwable cause) {
        super(message, cause);
    }

    public RepositoryAccessException(Throwable cause) {
        super(cause);
    }
}
